CREATE DATABASE IF NOT EXISTS kukano;
CREATE USER 'kukano'@'%' IDENTIFIED BY 'kukano';
GRANT ALL ON `kukano`.* TO 'kukano'@'%';

ALTER DATABASE kukano CHARACTER SET utf8 COLLATE utf8_general_ci;

use kukano;

CREATE TABLE incident(
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	timestamp DATETIME,
	picture TEXT,
	full_picture TEXT
);
