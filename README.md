# Kukano's backend

If you already noticed project [kukano](https://gitlab.com/kukano/rpi), you might be interested in its backend. The
main purpose is to provide CRUD operations on incidents. Also, I needed Android [app](https://gitlab.com/kukano/android-app).

The requirements are pretty simple, just run it in any container orchestrator. There is `docker-compose.yaml` and in 
`.gitlab-ci.yaml` you can get idea on howto deploy it into K8s.

## Configuration

It's in `kukano/configuration/configuration.yml` and it accepts only 2 options:

 * database_connection_string - sqlalchemy connection string (should have gone with Peewee but FastApi docu suggested sqlalchemy)
 * api_key - static api key for apps to authenticate to, I know it's a stupid way to do authentication but! this project
is a thing I stitched up during lazy sundays, don't judge!
   
## Docs

FastApi is pretty cool in the way the documentation is generated. Once you deploy the app, use URL with end `/docs` 
and you'll get swagger UI.
