from datetime import datetime
from pydantic import BaseModel


class Incident(BaseModel):
    id: int
    timestamp: datetime
    picture: str
    full_picture: str
