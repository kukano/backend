from datetime import datetime
from pydantic import BaseModel


class IncidentBase(BaseModel):
    pass


class IncidentCreate(IncidentBase):
    timestamp: datetime
    picture: str
    full_picture: str


class Incident(IncidentBase):
    id: int
    timestamp: datetime
    picture: str
    full_picture: str

    class Config:
        orm_mode = True
