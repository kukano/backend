from sqlalchemy import Column, Integer, DateTime, String
from kukano.database import Base


class Incident(Base):
    __tablename__ = "incident"

    id = Column(Integer, primary_key=True, index=True)
    timestamp = Column(DateTime)
    picture = Column(String)
    full_picture = Column(String)
