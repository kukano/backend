from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from starlette import status

from kukano.User import User
from kukano.configuration import Configuration

X_API_KEY = APIKeyHeader(name='x-api-Key')


def check_authentication_header(x_api_key: str = Depends(X_API_KEY)):
    if x_api_key == Configuration().api_key:
        return User({
            'id': 0,
            'user_type': "generic client",
        })
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid API Key",
    )
