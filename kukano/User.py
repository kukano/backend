from pydantic import BaseModel


class User(BaseModel):
    id: int
    user_type: str

    def __init__(
        self,
        data: dict
    ):
        super().__init__(**data)
