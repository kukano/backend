from datetime import datetime

from sqlalchemy.orm import Session

from kukano.models import Incident
from kukano.schemas import IncidentCreate


def get_incident_by_id(db: Session, id: int):
    return db.query(Incident).filter(Incident.id == id).first()


def delete_incident_by_id(db: Session, id: int):
    db.query(Incident).filter(Incident.id == id).delete()
    db.commit()


def get_incident_by_timestamp(db: Session, timestamp: datetime):
    return db.query(Incident).filter(Incident.timestamp == timestamp).first()


def get_incidents(db: Session):
    return db.query(Incident).all()


def create_incident(db: Session, incident: IncidentCreate):
    db_incident = Incident(timestamp=incident.timestamp, picture=incident.picture, full_picture=incident.full_picture)
    db.add(db_incident)
    db.commit()
    return db_incident
