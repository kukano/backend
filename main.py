from typing import List

from fastapi import FastAPI, Depends, HTTPException
from fastapi.responses import HTMLResponse

from sqlalchemy.orm import Session

from kukano import crud
from kukano.User import User
from kukano.database import Base, engine, SessionLocal
import kukano.schemas as schemas
from kukano.security import check_authentication_header

Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.get("/", response_class=HTMLResponse)
async def read_root(user: User = Depends(check_authentication_header)):
    return f"""
        Yahaa {user.id}
    """


@app.post("/incident/", response_model=schemas.Incident)
async def post_incident(incident: schemas.IncidentCreate, user: User = Depends(check_authentication_header), db: Session = Depends(get_db)):
    db_user = crud.get_incident_by_timestamp(db, timestamp=incident.timestamp)
    if db_user is not None:
        raise HTTPException(status_code=400, detail="Incident already noticed")
    return crud.create_incident(db=db, incident=incident)


@app.get("/incident/", response_model=List[schemas.Incident])
async def get_incidents(user: User = Depends(check_authentication_header), db: Session = Depends(get_db)):
    return crud.get_incidents(db=db)


@app.get("/incident/{incident_id}", response_model=schemas.Incident)
async def get_incident(incident_id: int, user: User = Depends(check_authentication_header), db: Session = Depends(get_db)):
    return crud.get_incident_by_id(db=db, id=incident_id)


@app.delete("/incident/{incident_id}", response_class=HTMLResponse)
async def delete_incident(incident_id: int, user: User = Depends(check_authentication_header), db: Session = Depends(get_db)):
    crud.delete_incident_by_id(db=db, id=incident_id)
    return f"Ok"


@app.get("/healthz/", response_class=HTMLResponse)
async def healthz():
    return f"Ok"
